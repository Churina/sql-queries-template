--1
INSERT INTO students(first_name, last_name, username, email)
VALUES('Dan','Zanes','dzanes','dzanes@college.edu');


--2
UPDATE students SET email = 'jblair21@college.edu'
WHERE id = 1;


--3
SELECT CONCAT(last_name,', ',first_name) AS "Name", email AS "Email"
FROM students
ORDER BY last_name;


--4
SELECT classes.name, grades.grade
FROM grades
INNER JOIN classes ON classes.id = grades.class_id
WHERE grades.student_id = 3;


--5
DELETE FROM grades WHERE student_id = 4;
DELETE FROM students WHERE id = 4;


--6
SELECT students.first_name ||' '||students.last_name AS "full name", grades.grade 
FROM grades
INNER JOIN students ON grades.student_id = students.id
WHERE grades.class_id = 1;


--7 Stretch

SELECT CONCAT(students.first_name,' ',students.last_name) AS "Student", classes.name AS "Class", grades.grade AS "Grade"
FROM grades
INNER JOIN classes ON grades.class_id = classes.id
INNER JOIN students ON grades.student_id = students.id;
